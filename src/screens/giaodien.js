import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  SafeAreaView,
  placeholder
} from "react-native";
import AppDimensions from "../utils/AppDimensions";

export default class MainScreen extends Component {
  wordIsMemorized = word => {
    if (word.isMemorized) {
      return "---";
    } else {
      return word.vn;
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      text: "English"
    };
  }
  render() {
    const word = {
      en: "One",
      vn: "Một",
      isMemorized: true
    };
    return (
      // <View
      //   style={{
      //     flex: 1,
      //     flexDirection: 'row',
      //     justifyContent: 'space-evenly',
      //   }}
      // >
      //   <Text
      //     style={{color: '#28a845', fontSize: AppDimensions.getWidth() / 20}}
      //   >
      //     {word.en}
      //   </Text>
      //   <Text
      //     style={{color: '#dd3545', fontSize: AppDimensions.getWidth() / 20}}
      //   >
      //     {this.wordIsMemorized(word)}
      //   </Text>
      // </View>
      <View
        style={{
          flex: 1
        }}
      >
        <View style={css.tieude}>
          <View style={css.tieude1}>
            <TextInput
              style={css.textInput1}
              placeholder="English"
              onChangeText={text =>
                this.setState({
                  text
                })
              }
              value={this.state}
            />{" "}
          </View>{" "}
          <View style={css.tieude2}>
            <TextInput
              style={css.textInput1}
              placeholder="Vietnamese"
              onChangeText={text =>
                this.setState({
                  text
                })
              }
              value={this.state}
            />{" "}
          </View>{" "}
          <View
            style={{
              marginTop: 20,
              flex: 1,
              flexDirection: "row"
            }}
          >
            <View style={css.bgAdd}>
              <Text style={css.textAdd}> Add word </Text>{" "}
            </View>{" "}
            <View style={css.bgCan}>
              <Text style={css.textCancel}> Cancel </Text>{" "}
            </View>{" "}
          </View>{" "}
        </View>{" "}
        <View
          style={{
            flex: 1
          }}
        >
          <View style={css.meMo1}>
            <View
              style={{
                flex: 0.5
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 40
                }}
              >
                {" "}
                A{" "}
              </Text>{" "}
              <View style={css.memorized}>
                <Text style={css.textAdd}> Memorized </Text>{" "}
              </View>{" "}
            </View>{" "}
            <View
              style={{
                flex: 0.5
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  marginTop: 40
                }}
              >
                {" "}
                B{" "}
              </Text>{" "}
              <View style={css.remove}>
                <Text style={css.textAdd}> Remove </Text>{" "}
              </View>{" "}
            </View>{" "}
          </View>{" "}
          <View style={css.meMo2}>
            <View
              style={{
                flex: 0.5
              }}
            >
              <View style={css.memorized}>
                <Text style={css.textAdd}> Memorized </Text>{" "}
              </View>{" "}
            </View>{" "}
            <View
              style={{
                flex: 0.5
              }}
            >
              <View style={css.remove}>
                <Text style={css.textAdd}> Remove </Text>{" "}
              </View>{" "}
            </View>{" "}
          </View>{" "}
        </View>{" "}
      </View>
    );
  }
}
var css = StyleSheet.create({
  meMo1: {
    flex: 0.5,
    backgroundColor: "#f0f0f0",
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 10
  },
  meMo2: {
    flex: 0.4,
    backgroundColor: "#f0f0f0",
    justifyContent: "center",
    flexDirection: "row",
    marginBottom: 10
  },
  bgAdd: {
    backgroundColor: "#28a745",
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    borderRadius: 5
  },
  memorized: {
    backgroundColor: "#dc3545",
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    marginLeft: 50,
    marginRight: 50,
    borderRadius: 5,
    marginTop: 25
  },
  remove: {
    backgroundColor: "#ffc107",
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    marginRight: 50,
    borderRadius: 5,
    marginTop: 25
  },
  bgCan: {
    backgroundColor: "#dc3545",
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    marginRight: 50,
    borderRadius: 5
  },
  textAdd: {
    color: "white"
  },
  textCancel: {
    color: "white"
  },
  tieude: {
    flex: 1,
    backgroundColor: "#f0f0f0",
    marginBottom: 100
  },
  textInput1: {
    height: 40,
    borderColor: "#ced4da",
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10
  },
  tieude1: {
    marginTop: 50,
    backgroundColor: "white",
    marginLeft: 20,
    marginRight: 20
  },
  tieude2: {
    marginTop: 20,
    backgroundColor: "white",
    marginLeft: 20,
    marginRight: 20
  }
});
//   constructor(props) {
//     super(props);
//     this.state = {
//       somayman: 999,
//     };
//   }
//   clickMe() {
//     console.log('click me');
//     this.setState({
//       somayman: this.state.somayman + 1,
//     });
//   }
// <View>
// <Text style={ao.tieude}>Hello 1</Text>
// <Text style={ao.tieude1}>Hello 1</Text>
// </View>
// var ao = StyleSheet.create({
//   tieude: {
//     backgroundColor: 'red',
//     color: 'white',
//   },
//   tieude1: {
//     color: 'yellow',
//   },
// });

// touchable
//   <View>
//     <TouchableOpacity
//       onPress={() => {
//         console.log('You Click me');
//       }}
//     >
//       <View
//         style={{
//           backgroundColor: 'green',
//           width: 100,
//           height: 20,
//           marginTop: 100,
//           marginLeft: 20,
//         }}
//       >
//         <Text />
//       </View>
//     </TouchableOpacity>
//   </View>

// <View>
// <Text style={{fontSize: 100}}>{this.state.somayman}</Text>
// <TouchableOpacity
// onPress={() => {
//     this.clickMe();
// }}
// >
// <View style={{width: 100, height: 20, backgroundColor: 'green'}} />
// </TouchableOpacity>
// </View>
