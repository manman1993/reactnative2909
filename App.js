/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { Text, View, Platform, SafeAreaView, Dimensions } from "react-native";
import MainScreen from "./src/screens/MainScreen";
import Box from "./src/components/Box";
export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <MainScreen />
      </SafeAreaView>
    );
  }
}
// Flex basic : set cứng
// FlexGrow
// <View style={{marginTop: Platform.OS === "ios" ? 150 : 0}}>
//   <View style={{backgroundColor: "#f20"}}>
//     <Text
//       style={{
//         fontSize: 40,
//         fontWeight: "800",
//         borderRadius: 25,
//         borderWidth: 5,
//       }}
//     >
//       textInComponentt
//     </Text>
//   </View>
// </View>
// <View
//   style={{
//     marginTop: 50,
//     flex: 1,
//     flexDirection: "row",
//     backgroundColor: "yellow",
//   }}
// >
//   <Text style={{flex: 1, backgroundColor: "red"}}>View 1</Text>
//   <Text style={{flex: 1, backgroundColor: "green"}}>View 2</Text>
//   <Text style={{width: 200, backgroundColor: "blue"}}>View 3</Text>
// </View>

// Bài 1
// <SafeAreaView style={{flex: 1, flexDirection: "column"}}>
//   <View style={{flex: 1}}>
//     <View
//       style={{
//         flexDirection: "column",
//         flex: 1,
//         backgroundColor: "red",
//         justifyContent: "center",
//       }}
//     >
//       <Text
//         style={{
//           textAlign: "center",
//         }}
//       >
//         A
//       </Text>
//     </View>
//     <View
//       style={{
//         flexDirection: "column",
//         flex: 1,
//         backgroundColor: "#359b2e",
//         justifyContent: "center",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>B</Text>
//     </View>
//     <View
//       style={{
//         flexDirection: "column",
//         flex: 1,
//         backgroundColor: "#7f9e12",
//         justifyContent: "center",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>C</Text>
//     </View>
//     <View
//       style={{
//         flexDirection: "column",
//         flex: 1,
//         backgroundColor: "#4048eb",
//         justifyContent: "center",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>D</Text>
//     </View>
//   </View>

//   <View style={{flex: 1, flexDirection: "row"}}>
//     <View
//       style={{
//         flex: 1,
//         backgroundColor: "#d2c964",
//         alignItems: "center",
//         justifyContent: "center",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>E</Text>
//     </View>
//     <View
//       style={{
//         flex: 1,
//         backgroundColor: "#818f78",
//         alignItems: "center",
//         justifyContent: "center",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>F</Text>
//     </View>
//     <View
//       style={{
//         alignItems: "center",
//         justifyContent: "center",
//         flex: 1,
//         backgroundColor: "#8127bd",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>G</Text>
//     </View>
//     <View
//       style={{
//         alignItems: "center",
//         justifyContent: "center",
//         flex: 1,
//         backgroundColor: "#5c1341",
//       }}
//     >
//       <Text style={{textAlign: "center"}}>H</Text>
//     </View>
//   </View>
// </SafeAreaView>

//** bài 2 */
// <
// SafeAreaView style = {
//   {
//     flex: 12
//   }
// } >
// <
// View style = {
//   {
//     flex: 4,
//     backgroundColor: "red",
//     justifyContent: "center",
//     alignItems: "center",
//     borderRadius: 0,
//     borderWidth: 3,
//   }
// } >
// <
// Text style = {
//   {
//     textAlign: "center",
//     color: "white",
//     fontWeight: "bold",
//   }
// } >
// 1 <
// /Text> <
// /View> <
// View style = {
//   {
//     flex: 8,
//     flexDirection: "row"
//   }
// } >
// <
// View style = {
//   {
//     flex: 2,
//     backgroundColor: "#fef102",
//     justifyContent: "center",
//     alignItems: "center",
//     borderRightColor: "black",
//     borderLeftWidth: 3,
//     borderRightWidth: 3,
//     borderBottomWidth: 3,
//   }
// } >
// <
// Text style = {
//   {
//     color: "white",
//     fontWeight: "bold"
//   }
// } > 2 < /Text> <
// /View> <
// View style = {
//   {
//     flex: 6,
//   }
// } >
// <
// View style = {
//   {
//     flex: 4,
//     backgroundColor: "#00a2ea",
//     justifyContent: "center",
//     alignItems: "center",
//     borderRightWidth: 3,
//     borderBottomWidth: 3,
//   }
// } >
// <
// Text style = {
//   {
//     color: "white",
//     fontWeight: "bold"
//   }
// } > 3 < /Text> <
// /View> <
// View style = {
//   {
//     flex: 2,
//     flexDirection: "row"
//   }
// } >
// <
// View style = {
//   {
//     flex: 2,
//     backgroundColor: "#feaec9",
//     justifyContent: "center",
//     alignItems: "center",
//     borderRightWidth: 3,
//     borderBottomWidth: 3,
//   }
// } >
// <
// Text style = {
//   {
//     color: "white",
//     fontWeight: "bold"
//   }
// } > 4 < /Text> <
// /View> <
// View style = {
//   {
//     flex: 2,
//     backgroundColor: "#a349a3",
//     justifyContent: "center",
//     alignItems: "center",
//     borderRightWidth: 3,
//     borderBottomWidth: 3,
//   }
// } >
// <
// Text style = {
//   {
//     color: "white",
//     fontWeight: "bold"
//   }
// } > 5 < /Text> <
// /View> <
// /View> <
// /View> <
// /View> <
// /SafeAreaView>

// Bài 3

// <SafeAreaView
//   style={{
//     flex: 12,
//   }}>
//   <View
//     style={{
//       flex: 6,
//       flexDirection: 'row',
//     }}>
//     <View
//       style={{
//         flex: 1.5,
//         backgroundColor: '#21d128',
//         justifyContent: 'center',
//         alignItems: 'center',
//       }}>
//       <Text> 1 </Text>{' '}
//     </View>{' '}
//     <View
//       style={{
//         flex: 4.5,
//         flexDirection: 'column',
//       }}>
//       <View
//         style={{
//           flex: 4.5,
//           backgroundColor: 'red',
//           justifyContent: 'center',
//           alignItems: 'center',
//         }}>
//         <Text> 3 </Text>{' '}
//       </View>{' '}
//       <View
//         style={{
//           flex: 4.5,
//           backgroundColor: '#cc8279',
//           justifyContent: 'center',
//           alignItems: 'center',
//         }}>
//         <Text> 4 </Text>{' '}
//       </View>{' '}
//     </View>{' '}
//   </View>
//   <View
//     style={{
//       flex: 6,
//       flexDirection: 'row',
//     }}>
//     <View
//       style={{
//         flex: 1.5,
//         backgroundColor: '#bac755',
//         justifyContent: 'center',
//         alignItems: 'center',
//       }}>
//       <Text> 2 </Text>{' '}
//     </View>{' '}
//     <View
//       style={{
//         flex: 4.5,
//         flexDirection: 'row',
//       }}>
//       <View
//         style={{
//           flex: 3,
//         }}>
//         <View
//           style={{
//             flex: 1.5,
//             backgroundColor: '#3688b8',
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}>
//           <Text> 5 </Text>{' '}
//         </View>{' '}
//         <View
//           style={{
//             flex: 1.5,
//             backgroundColor: '#6d2c66',
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}>
//           <Text> 6 </Text>{' '}
//         </View>{' '}
//       </View>{' '}
//       <View
//         style={{
//           flex: 1.5,
//         }}>
//         <View
//           style={{
//             flex: 0.5,
//             backgroundColor: '#6d2c66',
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}>
//           <Text> 7 </Text>{' '}
//         </View>{' '}
//         <View
//           style={{
//             flex: 0.5,
//             backgroundColor: '#b77624',
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}>
//           <Text> 8 </Text>{' '}
//         </View>{' '}
//         <View
//           style={{
//             flex: 0.5,
//             backgroundColor: '#998f98',
//             justifyContent: 'center',
//             alignItems: 'center',
//           }}>
//           <Text> 9 </Text>{' '}
//         </View>{' '}
//       </View>{' '}
//     </View>{' '}
//   </View>{' '}
// </SafeAreaView>
// <SafeAreaView>
//   <Text style={{fontSize: Dimensions.get("window").width / 2}}>
//     View 1
//   </Text>
// </SafeAreaView>
